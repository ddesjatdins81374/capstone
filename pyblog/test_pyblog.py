import unittest
from unittest import mock
from pyblog import get_headers, read_post, blog_post
import os.path
import os
import sys
import argparse
import base64

good_read_event = {'date': '2019-11-05T15:18:21',
'title': {'rendered': 'Latin is a fun language'},
'content': {'rendered': 'Lorem ipsum dolor sit amet'},
'excerpt': {'rendered': '<p>Lorem ipsum dolor sit amet'}
}


#Retrieving default environment variables with os module
user = os.environ.get('WORDPRESS_USERNAME')
password = os.environ.get('WORDPRESS_PASSWORD')
url = os.environ.get('WORDPRESS_URL')

headers = get_headers(user, password)


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        text = good_read_event
        content = good_read_event

        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

        def _iter__(self):
            return self

        def __next__(self):
            return self
    
    return MockResponse(kwargs["data"], args[0])


class TestAPI(unittest.TestCase):

    def test_headers(self):
        data_string = "user:password"
        encoded_creds = base64.b64encode(data_string.encode('utf-8'))
        expected_header = {'Authorization': 'Basic ' + encoded_creds.decode('utf-8')}
        test_headers = get_headers("user", "password")
        self.assertEqual (expected_header, test_headers)

    # We patch 'requests.get' with our own method. The mock object is passed in to our test case method.
    @mock.patch('requests.get', side_effect=mocked_requests_get(200, data=good_read_event))
    def test_read_post_200(self, mock_get):
        print(url)
        print(headers)
        response = read_post(url, headers)
        self.assertEqual(200, response.status_code, "verify 200 status code")

    @mock.patch('requests.get', side_effect=mocked_requests_get(400, data=good_read_event))
    def test_read_post_400(self, mock_get):
        response = read_post(url, headers)
        self.assertEqual(400, response.status_code,"verify 400 status code")

    @mock.patch('requests.get', side_effect=mocked_requests_get(500, data=good_read_event))
    def test_read_post_500(self, mock_get):
        response = read_post(url, headers)
        self.assertEqual(500, response.status_code,"verify 500 status code")

if __name__ == "__main__":
    # headers = pyblog.get_headers(user, password)
    unittest.main()
