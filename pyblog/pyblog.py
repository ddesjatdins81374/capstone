import requests
import base64
import os
import argparse
import datetime
from pprint import pprint


def blog_post(file, url, headers):
    with open(file) as file:
        title = file.readline()
        contents = file.read().replace('\n', '')
        today = str(datetime.datetime.now())
        post = {'date': (today),
                'title': (title),
                'content': (contents),
                'status': 'publish'
                }
        rpost = requests.post(url + '/posts', headers=headers, json=post)
        print(rpost.json())


def read_post(url, headers):
    try:
        rget = requests.get('http://18.225.5.214:8088/')

    except requests.ConnectionError:
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        print("\nSite is down\n")
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')

    rget = requests.get(url + '/posts', headers=headers)

    if rget.status_code == 401:
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        print('\nbad username or password, please try again\n')
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    else:
        posts = rget.json()
        pprint(posts)
    return rget


def get_headers(user, password):
    # login to wordpress api with user and password os environment variables
    # basic authorization token
    data_string = user + ':' + password
    token = base64.b64encode(data_string.encode())
    headers = {'Authorization': 'Basic ' + token.decode('utf-8')}
    return headers


def main():
    # setting default environment variables with os module
    user = os.environ.get('WORDPRESS_USERNAME')
    password = os.environ.get('WORDPRESS_PASSWORD')
    url = os.environ.get('WORDPRESS_URL')
    # adding arguments for reading blog or posting blog
    parser = argparse.ArgumentParser()
    parser.add_argument('user_input', type=str, choices=['read', 'upload'], help='returns \
     the most recent wordpress post,or creates new post')
    parser.add_argument('-f', help='provide name of file')
    args = parser.parse_args()
    read_or_post = args.user_input
    headers = get_headers(user, password)

    if read_or_post == 'upload':
        if args.f is None:
            print('Please provide valid filename')
        else:
            blog_post(args.f, url, headers)
    else:
        read_post(url, headers)


if __name__ == "__main__":
    main()
