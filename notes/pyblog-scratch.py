import requests
import json
import base64
import re
import os
import sys
from getpass import getpass
import argparse
import datetime

#Setting default envronment variables with os module
#based on user input
wpuserset = input('Please enter your Wordpress username: ')
os.environ['WORDPRESS_USERNAME'] = wpuserset
wppasswordset = getpass('Enter your Wordpress password: ')
os.environ['WORDPRESS_PASSWORD'] = wppasswordset
os.environ['WORDPRESS_URL'] = 'http://18.225.5.214:8088/wp-json/wp/v2'

#Retrieving default environment variables with os module
user = os.environ.get('WORDPRESS_USERNAME')
password = os.environ.get('WORDPRESS_PASSWORD')
url = os.environ.get('WORDPRESS_URL')

#login to wordpress api with user and password os environment variables, use
#basic authorization token
data_string = user + ':' + password
token = base64.b64encode(data_string.encode())
headers = {'Authorization': 'Basic ' + token.decode('utf-8')}


#adding arguments for reading blog or posting blog
parser = argparse.ArgumentParser()
parser.add_argument('user_input', type=str, choices=['read', 'upload'], help='returns the most recent wordpress post ,or creates new post depending on what user types in')
parser.add_argument('-f', help='provide name of file')
args = parser.parse_args()
 
read_or_post = args.user_input

def blog_post(file):
    with open (args.f) as file:
        title = file.readline()
        contents = file.read().replace('\n','')
        today = str(datetime.datetime.now())
        post = {'date': (today),
                'title': (title),
                'content': (contents),
                'status': 'publish'
                }
        rpost = requests.post(url + '/posts', headers=headers, json=post)
        print('\nYour post is published on ' + json.loads(rpost.content.decode('utf-8'))['link'])


def read_post():
    try:
        rget = requests.get('http://18.225.5.214:8088/')

    except requests.ConnectionError:
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        print("\nSite is down\n")
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')

    rget = requests.get(url + '/posts', headers=headers)

    if rget.status_code == 401:
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        print('\nbad username or password, please try again\n')
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    else:
        posts = json.loads(rget.content.decode('utf-8'))[0]
        date = posts['date']
        title = posts['title']['rendered']
        content = posts['content']['rendered']
        excerpt = posts['excerpt']['rendered']
        clean = re.compile('<.*?>')
        clean_content = re.sub(clean, '', content)
        clean_excerpt = re.sub(clean, '', excerpt)
        print('\nHere is the latest blog post from PeteDave wordpress: \n')
        print('Date: ' + date)
        print('Title: ' + title)
        print('Content: ' + clean_content)
        print('Excerpt: ' + clean_excerpt)
        print('---------------------------------------')


if __name__ == "__main__":

    if read_or_post == 'upload':
        if args.f==None:
            print('Please provide valid filename')
        else:
            blog_post(args.f)
    else:
        read_post()