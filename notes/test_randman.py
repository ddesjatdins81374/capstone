import unittest
from unittest import mock
import pyblog
import os.path
import os
import sys

good_read_event = {'headers': {'Access-Control-Allow-Origin': '*'}, 'statusCode': 200, 'body': [
    {'id': 54, 'date': '2019-11-01T09:28:11', 'date_gmt': '2019-11-01T09:28:11', 'guid': {
        'rendered': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/2019/11/01/the-best-song-in-the-world-2/'},
     'modified': '2019-11-01T13:29:37', 'modified_gmt': '2019-11-01T13:29:37', 'slug': 'the-best-song-in-the-world-2',
     'status': 'publish', 'type': 'post',
     'link': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/2019/11/01/the-best-song-in-the-world-2/',
     'title': {'rendered': 'The Best song in the world'}, 'content': {
        'rendered': '<p>This is a song about the best song in the world</p>\n<p>This song is not the best song in the world it is about the best song in the world</p>\n',
        'protected': False}, 'excerpt': {
        'rendered': '<p>This is a song about the best song in the world This song is not the best song in the world it is about the best song in the world</p>\n',
        'protected': False}, 'author': 1, 'featured_media': 0, 'comment_status': 'open', 'ping_status': 'open',
     'sticky': False, 'template': '', 'format': 'standard', 'meta': [], 'categories': [1], 'tags': [],
     '_links': {'self': [{'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/54'}],
                'collection': [{'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts'}],
                'about': [{'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/types/post'}],
                'author': [{'embeddable': True,
                            'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/users/1'}],
                'replies': [{'embeddable': True,
                             'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/comments?post=54'}],
                'version-history': [{'count': 1,
                                     'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/54/revisions'}],
                'predecessor-version': [{'id': 55,
                                         'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/54/revisions/55'}],
                'wp:attachment': [
                    {'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/media?parent=54'}],
                'wp:term': [{'taxonomy': 'category', 'embeddable': True,
                             'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/categories?post=54'},
                            {'taxonomy': 'post_tag', 'embeddable': True,
                             'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/tags?post=54'}],
                'curies': [{'name': 'wp', 'href': 'https://api.w.org/{rel}', 'templated': True}]}}]}
good_upload_event = {'headers': {'Access-Control-Allow-Origin': '*'}, 'statusCode': 201,
                     'body': {'message': 'Posting complete',
                              'location': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/2019/11/01/the-best-song-in-the-world/',
                              'raw_json': {'id': 57, 'date': '2019-11-01T10:32:00', 'date_gmt': '2019-11-01T10:32:00',
                                           'guid': {
                                               'rendered': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/2019/11/01/the-best-song-in-the-world/',
                                               'raw': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/2019/11/01/the-best-song-in-the-world/'},
                                           'modified': '2019-11-01T10:32:00', 'modified_gmt': '2019-11-01T10:32:00',
                                           'password': '', 'slug': 'the-best-song-in-the-world', 'status': 'publish',
                                           'type': 'post',
                                           'link': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/2019/11/01/the-best-song-in-the-world/',
                                           'title': {'raw': 'The Best song in the world',
                                                     'rendered': 'The Best song in the world'}, 'content': {
                                               'raw': 'This is a song about the best song in the world\n\n\n\nThis song is not the best song in the world it is about the best song in the world\n',
                                               'rendered': '<p>This is a song about the best song in the world</p>\n<p>This song is not the best song in the world it is about the best song in the world</p>\n',
                                               'protected': False, 'block_version': 0}, 'excerpt': {'raw': '',
                                                                                                    'rendered': '<p>This is a song about the best song in the world This song is not the best song in the world it is about the best song in the world</p>\n',
                                                                                                    'protected': False},
                                           'author': 1, 'featured_media': 0, 'comment_status': 'open',
                                           'ping_status': 'open', 'sticky': False, 'template': '', 'format': 'standard',
                                           'meta': [], 'categories': [1], 'tags': [],
                                           'permalink_template': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/2019/11/01/%postname%/',
                                           'generated_slug': 'the-best-song-in-the-world', '_links': {'self': [{
                                               'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/57'}],
                                               'collection': [{
                                                   'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts'}],
                                               'about': [{
                                                   'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/types/post'}],
                                               'author': [{
                                                   'embeddable': True,
                                                   'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/users/1'}],
                                               'replies': [{
                                                   'embeddable': True,
                                                   'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/comments?post=57'}],
                                               'version-history': [
                                               {'count': 0,
                                                'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/57/revisions'}],
                                               'wp:attachment': [
                                               {
                                                   'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/media?parent=57'}],
                                               'wp:term': [{
                                                   'taxonomy': 'category',
                                                   'embeddable': True,
                                                   'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/categories?post=57'},
                                               {
                                                   'taxonomy': 'post_tag',
                                                   'embeddable': True,
                                                   'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/tags?post=57'}],
                                               'wp:action-publish': [
                                               {
                                                   'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/57'}],
                                               'wp:action-unfiltered-html': [
                                               {
                                                   'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/57'}],
                                               'wp:action-sticky': [
                                               {
                                                   'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/57'}],
                                               'wp:action-assign-author': [
                                               {
                                                   'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/57'}],
                                               'wp:action-create-categories': [
                                               {
                                                   'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/57'}],
                                               'wp:action-assign-categories': [
                                               {
                                                   'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/57'}],
                                               'wp:action-create-tags': [
                                               {
                                                   'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/57'}],
                                               'wp:action-assign-tags': [
                                               {
                                                   'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/57'}],
                                               'curies': [
                                               {'name': 'wp',
                                                'href': 'https://api.w.org/{rel}',
                                                'templated': True}]}}}}

bad_upload_event = {'id': 98, 'date': '2019-11-04T09:34:22', 'date_gmt': '2019-11-04T09:34:22', 'guid': {'rendered': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/2019/11/04/the-best-song-in-the-world-11/', 'raw': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/2019/11/04/the-best-song-in-the-world-11/'}, 'modified': '2019-11-04T09:34:22', 'modified_gmt': '2019-11-04T09:34:22', 'password': '', 'slug': 'the-best-song-in-the-world-11', 'status': 'publish', 'type': 'post', 'link': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/2019/11/04/the-best-song-in-the-world-11/', 'title': {'raw': 'The Best song in the world', 'rendered': 'The Best song in the world'}, 'content': {'raw': 'This is a song about the best song in the world\n\n\n\nThis song is not the best song in the world it is about the best song in the world\n', 'rendered': '<p>This is a song about the best song in the world</p>\n<p>This song is not the best song in the world it is about the best song in the world</p>\n', 'protected': False, 'block_version': 0}, 'excerpt': {'raw': '', 'rendered': '<p>This is a song about the best song in the world This song is not the best song in the world it is about the best song in the world</p>\n', 'protected': False}, 'author': 1, 'featured_media': 0, 'comment_status': 'open', 'ping_status': 'open', 'sticky': False, 'template': '', 'format': 'standard', 'meta': [], 'categories': [1], 'tags': [], 'permalink_template': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/2019/11/04/%postname%/', 'generated_slug': 'the-best-song-in-the-world-11', '_links': {'self': [{'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/98'}], 'collection': [{'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts'}], 'about': [{'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/types/post'}],
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  'author': [{'embeddable': True, 'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/users/1'}], 'replies': [{'embeddable': True, 'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/comments?post=98'}], 'version-history': [{'count': 0, 'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/98/revisions'}], 'wp:attachment': [{'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/media?parent=98'}], 'wp:term': [{'taxonomy': 'category', 'embeddable': True, 'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/categories?post=98'}, {'taxonomy': 'post_tag', 'embeddable': True, 'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/tags?post=98'}], 'wp:action-publish': [{'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/98'}], 'wp:action-unfiltered-html': [{'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/98'}], 'wp:action-sticky': [{'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/98'}], 'wp:action-assign-author': [{'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/98'}], 'wp:action-create-categories': [{'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/98'}], 'wp:action-assign-categories': [{'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/98'}], 'wp:action-create-tags': [{'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/98'}], 'wp:action-assign-tags': [{'href': 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088/wp-json/wp/v2/posts/98'}], 'curies': [{'name': 'wp', 'href': 'https://api.w.org/{rel}', 'templated': True}]}}
os.environ['WORDPRESS_USERNAME'] = 'jondy'
os.environ['WORDPRESS_PASSWORD'] = 'Randy'
os.environ['WORDPRESS_URL'] = 'http://ec2-35-153-160-157.compute-1.amazonaws.com:8088'
file_name = '/upload/file.tmp'
wordpress_username = os.getenv('WORDPRESS_USERNAME')
wordpress_password = os.getenv('WORDPRESS_PASSWORD')
wordpress_url = os.getenv('WORDPRESS_URL')


def mocked_requests_get(*args, **kwargs):
    class MockResponse:
        text = "mocked_requests_get"

        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

        def __iter__(self):
            return self

        def __next__(self):
            return self

    return MockResponse(kwargs["data"], args[0])


def mocked_requests_post(*args, **kwargs):
    class MockResponse:
        text = "mocked_requests_post"

        def __init__(self, json_data, status_code):
            self.json_data = json_data
            self.status_code = status_code

        def json(self):
            return self.json_data

        def __iter__(self):
            return self

        def __next__(self):
            return self

    return MockResponse(kwargs["data"], args[0])


class TestAPI(unittest.TestCase):

    # We patch 'requests.get' with our own method. The mock object is passed in to our test case method.
    @mock.patch('requests.get', side_effect=mocked_requests_get(200, data=good_read_event))
    def test_read_latest_200(self, mock_get):
        response = pyblog.read_latest(wordpress_url, headers)
        self.assertEqual(200, response["statusCode"], "verify 200 status code")

    @mock.patch('requests.get', side_effect=mocked_requests_get(400, data=good_read_event))
    def test_read_latest_400(self, mock_get):
        response = pyblog.read_latest(wordpress_url, headers)
        self.assertEqual(response["statusCode"], 400, "verify 400 status code")

    @mock.patch('requests.get', side_effect=mocked_requests_get(500, data=good_read_event))
    def test_read_latest_500(self, mock_get):
        response = pyblog.read_latest(wordpress_url, headers)
        self.assertEqual(response["statusCode"], 500, "verify 500 status code")

    @mock.patch('requests.post', side_effect=mocked_requests_post(201, data=good_upload_event))
    def test_upload_200(self, mock_post):
        response = pyblog.upload(file_name, wordpress_url, headers)
        print(response)
        self.assertEqual(response["statusCode"], 201, "verify 200 status code")

    @mock.patch('requests.post', side_effect=mocked_requests_post(400, data=bad_upload_event))
    def test_upload_400(self, mock_post):
        response = pyblog.upload(file_name, wordpress_url, headers)
        self.assertEqual(response["statusCode"], 400, "verify 400 status code")

    def test_main(self):
        sys.argv = ['pyblog.py', 'upload', '-f', 'file.tmp']
        pyblog.main(wordpress_username, wordpress_password, wordpress_url)


if __name__ == '__main__':
    headers = pyblog._create_headers(wordpress_username, wordpress_password)
    unittest.main()
