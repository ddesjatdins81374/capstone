import unittest
import os
import requests

from pyblog import read_post_file, make_post, latest_post

class TestPyblog(unittest.TestCase):

    def test_reading_file_title(self):
        file = 'test.txt'

        title, body = read_post_file(file)

        self.assertEqual('Title\n', title)
    

    def test_reading_file_body(self):
        file = 'test.txt'

        title, body = read_post_file(file)

        self.assertEqual('Body Text', body)

